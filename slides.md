## Revue 2.0

#### Journées plénières

&nbsp;

7 et 8 oct. 2019 | Montréal

<!-- .element: style="font-size:1.5rem" -->

[wiki.revue20.org](http://wiki.revue20.org) | [ecrinum.frama.io/s_r20pleniere2019](http://ecrinum.frama.io/s_r20pleniere2019)

<!-- .element: style="font-size:1.5rem" -->

---



![logo CRSH](img/crsh.png) <!-- .element: class="logo" style="height:50px; background-color:ghostwhite;padding: 4px" -->
![logo CRCEN](img/LogoENDT10-2016.png) <!-- .element: class="logo" style="height:50px; background-color:ghostwhite;padding: 4px" --> <br/>![CC-BY-SA](http://i.creativecommons.org/l/by-sa/4.0/88x31.png)

===

%%%%%%%%%%%%%%%%%%SECTIONmoveRight%%%%%%%%%%%%%%%%%%

## Plénière
<!-- .element: style="width:50%; font-size:1.2em; float:left; color:orange" -->

- Contexte
- Tour de table
- Déroulé et objectifs

<!-- .element: style="font-size:0.8em; width:45%; float:left;border-left:1px,solid,white;" -->


§§§§§SLIDEmoveDown§§§§§

### Contexte

> Repenser la mission des revues savantes en SHS

&nbsp;

<i class="fa fa-arrow-right"></i>  quels modèles épistémologiques ?

<!-- .element: style="font-size:0.9em" -->

<i class="fa fa-arrow-right"></i>  quels modèles éditoriaux ?

<!-- .element: style="font-size:0.9em" -->

===

- comment l'environnemnt numérique peut être mis au service d'un renouvellement de la vocation des revues ?

### Pistes théoriques
- la piste principale pour nous, c'est la conversation.
  la revue, à l'époque où la possibilité de diffusion des contenus est mulitipliée par les possibilités du numérique, la piste, c'est d'aller revoir la vocation initiale des revues : d'être un espace pour la conversation savante.
- la revue ne doit pas le lieu de la diffusion des résultats de recherche, c'est le lieu de la production des résultats de recherche de la communauté, surtout en shs.

- la piste de l'espace public

### un enjeu dans R20
- un des enjeux est celui de capacitation, d'empowerment, de littératie : accompagner les revues et leurs auteurs dans un apprentissage ciblé.

§§§§§SLIDEmoveDown§§§§§

### Le partenariat


<i class="fa fa-arrow-right"></i>  des revues

<!-- .element: style="font-size:0.9em" -->

<i class="fa fa-arrow-right"></i>  les diffuseurs

<!-- .element: style="font-size:0.9em" -->

<i class="fa fa-arrow-right"></i>  des chercheurs

<!-- .element: style="font-size:0.9em" -->

&nbsp;

![cybergeo](./img/cybergeo.png) <!-- .element: style="height:40px;background:whitesmoke;border:1px" -->
![etudefrancaise](./img/etudesFrancaises.png) <!-- .element: style="height:40px;background:whitesmoke;border:1px" -->
![intermedialites](./img/intermedialites.png)<!-- .element: style="height:40px;background:whitesmoke;border:1px" -->
![itineraires](./img/itineraires.png) <!-- .element: style="height:40px;background:whitesmoke;border:1px" -->
![memoireLivre](./img/memoiresLivre.jpg) <!-- .element: style="height:40px;background:whitesmoke;border:1px" -->
![Photolitterature](./img/phlit.jpg) <!-- .element: style="height:40px;background:whitesmoke;border:1px" -->
![erudit](./img/erudit.png) <!-- .element: style="padding:3px;height:40px;background:whitesmoke;border:1px" -->
![openedition](./img/logo_openedition.png) <!-- .element: style="padding:3px;height:40px;background:whitesmoke;border:1px" -->
![humanum](./img/humanum.png) <!-- .element: style="padding:3px;height:40px;background:whitesmoke;border:1px" -->



===

ce n'est pas qu'une chose technique, ni qu'une chose savante, il faut mettre ensemble ces deux pôles de la recherche.

§§§§§SLIDEmoveDown§§§§§

![calendrier-revue20](img/calendrieractivites.png)
<!-- .element: style="background-color:ghostwhite; padding:0.1rem 0.3rem;height:40rem;" -->

§§§§§SLIDEmoveDown§§§§§
### Planning

|périodes | phases|
|:--|:--|
|oct. 2018 | Lancement|
|oct. 2018 - sept 2019 | Observation des pratiques|
|oct. - déc.  2019 | Conception   |
|jan. - déc 2020   |Expérimentations   |
|jan. 2021 - sept 2021  |Finalisations   |

===



§§§§§SLIDEmoveDown§§§§§

### Activités sur le projet

 | |
:--|:--|
automne&nbsp;2018   | conception protocole observation   |
déc 2018| séance séminaire Pierre L. |
jan-mars&nbsp;2019  | formations Stylo et chaîne éditoriale: étudiants, bibliothèque
jan-juin 2019| observation des revues (CRC ecrinum) |
printemps&nbsp;2019| observation Huma-Num (Jasmine D.W.) |
avr 2019| séance séminaire Susan B. |
avr 2019 | hackathon Stylo/Erudit |
avr-mai 2019 | formation et expérimentations avec les revues _Études françaises_, _Intermédialités_, _Méta_ |
juin 2019| colloques CCA Vancouver 2019, NumRev Montpellier 2019 |
automne 2019| séminaire IEML par Pierre L. |
oct. 2019   | hackathon Stylo/Huma-Num

<!-- .element: style="font-size:0.6em;" -->

===

- recherche continue au sein de Sens public
  - thèse Nicolas S.
  - recrutement Antoine F.


§§§§§SLIDEmoveDown§§§§§
### Tour de table - revues

1. Études françaises (Stéphane Vachon et Fabrice C. Bergeron)
2. Intermédialités (Marion Froger et Maude Trottier)
3. Itinéraires (Magali Nachtergael et François-Xavier Mas)
4. Mémoires du livre (Anthony Glinoer)
5. *Revue internationale de photolittérature (Servanne Monjour)*
6. *Cybergéo (Christine Kosmopoulos)*
7. Captures (Bertrand Gervais)
8. Humanités Numériques (Emmanuel Chateau-Dutier)

<!-- .element: style="font-size:1.7rem" -->


§§§§§SLIDEmoveDown§§§§§

### Tour de table - institutions

1. OpenEdition (Marie Pellen)
2. Huma-num (Stéphane Pouyllau)
3. Érudit (Tanja Niemann et Émilie Paquin)

<!-- .element: style="font-size:1.7rem" -->

§§§§§SLIDEmoveDown§§§§§

### Tour de table - chercheurs

1. Maude Bonenfant (Université du Québec à Montréal)
2. Renée Bourassa (Université Laval)
3. Susan Brown (University of Guelph)
4. Emmanuel Château-Dutier (Université de Montréal)
5. Constance Crompton (Université d’Ottawa)
6. Juliette De Maeyer (Université de Montréal)
7. Michael Eberle-Sinatra (Université de Montréal)
8. Bertrand Gervais (Université du Québec à Montréal)
9. Vincent Larivière (Université de Montréal)
10. Pierre Lévy (Université d’Ottawa)
11. Benoît Melançon (Université de Montréal)
12. Servanne Monjour (Sorbonne Universités)
13. Stéfan Sinclair (McGill University)
14. Jasmine Drudge-Willson (University of Guelph)
15. Louis van Beurden (Université de Montréal)

<!-- .element: style="font-size:1.7rem" -->



§§§§§SLIDEmoveDown§§§§§

### Déroulé des journées

||**Jour 1**||
|:--|:--|--:|
| 9h | Plénière & retour sur Observation |  3h |
| 12h | _déjeuner_| |
| 13h | Atelier 1 : objectifs  | 1h30 |
| 14h30 | _pause_| |
| 15h | Atelier 2 : scénari d'expérimentation|  2h |
| 17h | _fin de la première journée_| |

<!-- .element: style="font-size:0.6em; width:45%; float:left;" -->


||**Jour 2**||
|:--|:--|--:|
| 9h | Atelier 3 : Conception des expérimentations |  2h30 |
| 11h30 | Mise en commun et calendrier |  30min |
| 12h | _déjeuner_| |
| 13h | Atelier 4 : Demande Partenariat | 3h |
| 14h30 | _pause_| |
| 16h | _fin de la seconde journée_| |

<!-- .element: style="font-size:0.6em; width:45%; float:left;margin-left:20px;border:1px,solid,white;" -->

===

objectif : on démarre la recherche aujourd'hui

- schedule
- objectif de ateliers




%%%%%%%%%%%%%%%%%%SECTIONmoveRight%%%%%%%%%%%%%%%%%%

## Atelier&nbsp;1 Objectifs fondamentaux
<!-- .element: style="width:50%; font-size:1.2em; float:left; color:orange" -->

- Objectifs de l'atelier
- Axes de réflexion
  - production
  - légitimation
  - diffusion
  - appropriation
- Échange collectif

<!-- .element: style="font-size:0.8em; width:45%;border-left:1px,solid,white;" -->

===

- l'objectif de l'atelier est de dégager les objectifs fondamentaux du projet à partir des conclusions de la phase 1 d'observation, et d'identifier les grandes problématiques des revues à adresser lors des expérimentations.
- nous nous appuyons sur 4 axes de réflexion: production/évaluation-légitimation/diffusion/appropriation.
- l'atelier nous permettra d'orienter au plus près des besoins de chaque revue les scénari d'expérimentation.
- Une note servira à compiler en temps réel les besoins et les scénaris évoqués pendant cet échange.


§§§§§SLIDEmoveDown§§§§§

### Axes de réflexion

1. Production
2. Évaluation-légitimation
3. Diffusion
4. Appropriation

<!-- .element: style="font-size:0.8em;" -->

===

Nous allons passer "en revue" les éléments recueillis de la première phase, en les plaçant dans ces 4 axes de réflexion.
Cela doit nous amener à identifier les problématiques.
Nous allons vous solliciter pour chacun de ces axes, mais libre à vous de venir compléter nos propositions.

§§§§§SLIDEmoveDown§§§§§

#### Production

- Définition : détermination d'une structure et d'une forme, "production" d'un contenu en vue de sa publication. <!-- .element: class="fragment" data-fragment-index="1" -->
- Problématique soulevée : réflexion autour de l'enrichissement scientifique dans la phase de production. <!-- .element: class="fragment" data-fragment-index="2" -->

<!-- .element: style="font-size:0.8em;" -->


===

Par exemple Stylo remet dans les mains des éditeurs les outils pour produire, mais au-delà de cela qu'est-ce que peuvent faire les éditeurs des potentialités d'enrichissement de ces outils ?

Exemple dans le cas de la production ? Intermédialités ?

§§§§§SLIDEmoveDown§§§§§

#### Évaluation-légitimation

- Définition : analyser un article pour déterminer sa cohérence au sein d'une collection éditorialisée, déclarer la pertinence d'un contenu. <!-- .element: class="fragment" data-fragment-index="1" -->
- Problématique soulevée : comment mettre en phase les processus d'évaluation d'un texte avec les pratiques actuelles ? <!-- .element: class="fragment" data-fragment-index="2" -->

<!-- .element: style="font-size:0.8em;" -->


===

Exemple dans le cas de l'évaluation-légitimation ?

§§§§§SLIDEmoveDown§§§§§

#### Diffusion

- Définition : rendre disponible un article et l'adresser à un lecteur (création d'une relation entre le producteur de contenu et son destinataire). <!-- .element: class="fragment" data-fragment-index="1" -->
- Problématique soulevée : comment rendre possible une continuité depuis la production jusqu'à la diffusion ? Complémentarité entre référencement par et dans les moteurs de recherche généralistes et spécialisés, et référencement humain avec les communautés. <!-- .element: class="fragment" data-fragment-index="2" -->

<!-- .element: style="font-size:0.8em;" -->


===

La question de la continuité est la tension entre la production et la diffusion, en prenant en compte le fait que l'appropriation peut être réinjectée dans la production.

Exemple dans le cas de la diffusion ?

§§§§§SLIDEmoveDown§§§§§

#### Appropriation

- Définition : permettre l'interaction, le partage, la dissémination ou l'enrichissement d'un article au-delà de son accès. <!-- .element: class="fragment" data-fragment-index="1" -->
- Problématique soulevée : quels dispositifs mettre en place pour une appropriation des articles ? Quelle boucle de rétroaction pour exploiter cette appropriation ? <!-- .element: class="fragment" data-fragment-index="2" -->

<!-- .element: style="font-size:0.8em;" -->


===

La vraie question est celle de la fonction éditoriale : cela fait partie de la fonction des éditeurs de penser la condition de l'appropriation.

Exemple dans le cas de l'appropriation ?

§§§§§SLIDEmoveDown§§§§§

### Atelier&nbsp;1 Échange collectif

%%%%%%%%%%%%%%%%%%SECTIONmoveRight%%%%%%%%%%%%%%%%%%

## Atelier&nbsp;2 Scénari d'expérimentation
<!-- .element: style="width:45%; border-left:1px,solid,white; float:left; color:orange; font-size: 1.2em" -->

- Objectifs de l'atelier
- Problématiques et scénari correspondant
- Formation de groupes de travail (Atelier 3)

<!-- .element: style="font-size:0.8em; width:35%; float:left;padding-left:2rem;border-left:1px,solid,white;" -->

===

L'objectif de l'atelier est de faire le tour des problématiques et besoins identifié·e·s au précédent atelier, et de discuter de possibles scénari susceptible de les adresser


%%%%%%%%%%%%%%%%%%SECTIONmoveRight%%%%%%%%%%%%%%%%%%

## Atelier&nbsp;3 Groupes de travail par revue
<!-- .element: style="width:45%; border-left:1px,solid,white; float:left; color:orange; font-size: 1.2em" -->

- Objectifs de l'atelier
- Méthodologie et outputs
- Mise en commun
- Planification générale

<!-- .element: style="font-size:0.8em; width:45%; border-left:1px,solid,white;" -->


§§§§§SLIDEmoveDown§§§§§

### Méthodologie

un formulaire pour définir l'expérimentation :

|||
|:--|:--|
| besoins/problématiques adressées, objectifs| ... |
| difficultés et verrous anticipés| ... |
| personnes/partenaires ressources| ... |
| périmètre d'action| ... |
| méthodologie| ... |
| déroulé et calendrier 2019-2020| ... |

§§§§§SLIDEmoveDown§§§§§

### Groupes

- Groupe 1: {axe} {revue·s} {chercheurs}
- Groupe 2: {axe} {revue·s} {chercheurs}
- Groupe 3: {axe} {revue·s} {chercheurs}
- ...

§§§§§SLIDEmoveDown§§§§§

### Mise en commun et calendrier

%%%%%%%%%%%%%%%%%%SECTIONmoveRight%%%%%%%%%%%%%%%%%%

## Atelier&nbsp;4 Partenariat

<!-- .element: style="width:45%; border-left:1px,solid,white; float:left; color:orange; font-size: 1.2em" -->

1. Objectifs de l'atelier
2. Pitch et attentes des partenaires
3. Plan de travail et calendrier sur 7 ans
4. Requis pour janvier 2020
3. Rétro-planning jusqu'à oct. 2020

<!-- .element: style="width:50%; font-size:0.8em; float:left;" -->



%%%%%%%%%%%%%%%%%%SECTIONmoveRight%%%%%%%%%%%%%%%%%%

### Merci !


<i class="fa fa-arrow-right"></i> présentation : [ecrinum.frama.io/s_r20pleniere2019/](https://ecrinum.frama.io/s_r20pleniere2019/)

<!-- .element: style="font-size:0.7em;" -->


---
![logo CRCEN](img/LogoENDT10-2016.png) <!-- .element: class="logo" style="width:25%; background-color:ghostwhite;padding: 7px" -->
![CC-BY-SA](http://i.creativecommons.org/l/by-sa/4.0/88x31.png)

===

.slide: data-background-image="img/" data-background-size="cover"
.slide: class="hover"

<!-- .element: style="font-size:1.7rem" -->
<!-- .element: style="font-size:1.7rem; text-align:left; padding-left:1rem;" -->

pour baliser en inline
- yaml <!-- .element: style="color:Darkorange;" -->


pour faire deux colonnes
<!-- .element: style="font-size:0.6em; width:55%; float:left;" -->
<!-- .element: style="font-size:0.6em; width:35%; float:left;padding-left:2rem;border:1px,solid,white;" -->
